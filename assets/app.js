//Bouton qui ajoute des élements
let myBtn = document.getElementById("add");

    myBtn.addEventListener("click", function() {

    var ul = document.getElementById("todo");
    var li = document.createElement("li");
    var x = document.createElement("INPUT");
    var children = ul.children.length + 1
    li.setAttribute("id", "element"+children)
    x.setAttribute("type", "checkbox")
    x.setAttribute("id", "check"+children);
    li.append(document.getElementById("tache").value);
    document.getElementById("tache").value = "";
    ul.appendChild(li);
    li.appendChild(x);
    
//Bouton qui raye quand on click sur la checkbox
let check = document.getElementById("element"+children);

	check.addEventListener("click", function () {	
        if (document.getElementById("check"+children).checked) {
      	document.getElementById("element"+children).style.textDecoration = "line-through";
      } else {document.getElementById("element"+children).style.textDecoration = "none";
  }
})

//Bouton qui fait disparaitre que les elements cochés
let myBtnTodo = document.getElementById("btntodo");

    myBtnTodo.addEventListener("click", function() {
      if (document.getElementById("check"+children).checked) {
      	document.getElementById("element"+children).style.visibility = "hidden";
      }else{
        document.getElementById("element"+children).style.visibility = "visible"; 
      }
    })

//Bouton qui fait apparaitre que les elements cochés
let myBtnDone = document.getElementById("btndone");    

    myBtnDone.addEventListener("click", function() {
      if (document.getElementById("check"+children).checked) {
      	document.getElementById("element"+children).style.visibility = "visible";
      }else{
        document.getElementById("element"+children).style.visibility = "hidden"; 
      }
    })
    
//Bouton qui fait apparaitre tout les éléments
let myBtnAll = document.getElementById("btnall");  
myBtnAll.addEventListener("click", function() {
    if (document.getElementById("check"+children).checked) {
        document.getElementById("element"+children).style.visibility = "visible";
    }else{
      document.getElementById("element"+children).style.visibility = "visible"; 
    }
})

 });
